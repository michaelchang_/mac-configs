"UltiSnips

" obsession.vim
":Obsess to start recording a session file
":Obsess! to stop
"vim -S / :so to load session file

"exchange.vim
"cx{motion} then . to repeat
"cxx for current line
"cxc clear any {motion} for exchange

" QFEnter commands
" <leader>enter open in a new vertical split
" <leader>space open in horizontal split

" Ack commands
" :Ack
":AckAdd matches are appended to quickfix list
":AckFromSearch
":LAck matches are placed in location list
"O to open and close quickfix window
"v to open in vertical split

"syntastic commands
":SyntasticToggleMode - disable checking

"F5 TagBar
"F4 GoldenSplit
"C-o Swap with main pane
"C-n next pane
"C-p previous pane

"fzf commands
"Files - files
"Buffers - open buffers
"Ag [Pattern] - ag search result
"Lines/BLines [Query] - lines in loaded/current buffer
"Tags/BTags [Query]
"Helptags
"Ctrl-X/V to open in new split/vertical split
"Ctrl-J/K to move up and down in search results

"vim-plug
"PlugInstall, PlugUpdate, PlugStatus, PlugClean

"ListToggle
"<leader>l and <leader>q to toggle locationlist/quickfix
"let g:lt_height=10

"tabs/buffers
"gb gB
"gt gT to go to next/previous tab
"{i}gt to go to tab in position i
":ls to list buffers
":bd to close current buffer
":bd2 to close buffer 2

"Recover.vim
":FinishRecovery to keep swp version and delete the swp file
":diffoff! and :close to close the diff version and close the window that contains the on-disk version

"vim-trailing-whitespace
":FixWhitespace to fix trailing whitespace

"vim-unimpaired
"supports a count
"a for args, q for quickfix
"[a previous
"]a next
"[A first
"]A last
"works for b, l, t
"[q ]q [Q ]Q
"cprevious cnext cfirst clast
"[f ]f go to file preceding/succeeding current one
"[n ]n go to previous SCM conflict marker on diff/patch
"=p =P paste after and reindent
"[<space> ]<space> add blank line before/after cursor
"[e ]e exchange current line with [count] lines above/below
"also has xml/url/cstring encoding/decoding


"jdelkin's vim-correction
"based on abolish
"to turn off
"let g:loaded_autocorrect = 1
"to disable in a session
"set paste

"abolish.vim
"replace all variants of words
":%Subvert/facilit{y, ies}/building{,s}/g
"crs coerce to snake_case
"crm coerce to MixedCase
"crc coerce to camelCase
"cru coerce to UPPERCASE

"speeddating.vim
"C-A to increment date
"C-X to decrement date

"surround.vim commands
"change surrounding
"cs[original surrounding character][new surrounding character]
"delete surrounding
"ds[surrounding character]
"surround a word (iw is text object) with brackets
"ysiw]
"yss to wrap a line

"a.vim commands
":A switches to header file
"<leader>is
":AV vertical split and switches
":AT new tab and switches
"<leader>ih
":IH switch to file under cursor
":IHV vertical split and switch

set nocompatible              " be iMproved, required
set encoding=utf-8

call plug#begin('~/.vim/bundle')
Plug 'mileszs/ack.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'ciaranm/detectindent'
Plug 'vim-scripts/L9'
Plug 'Valloric/ListToggle'
Plug 'chrisbra/Recover.vim'
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-abolish'
"Plug 'vim-airline/vim-airline'
"Plug 'bling/vim-bufferline'
Plug 'tpope/vim-repeat'
Plug 'bronson/vim-trailing-whitespace'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-sensible'
Plug 'jdelkins/vim-correction'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'

Plug 'junegunn/fzf', { 'dir' : '~/.fzf', 'do' : './install -all'  }
Plug 'junegunn/fzf.vim'

Plug 'chip/vim-fat-finger'
Plug 'kshenoy/vim-signature'
Plug 'tpope/vim-fugitive'
" Plug 'tpope/vim-obsession'
Plug 'majutsushi/tagbar'
Plug 'zhaocai/GoldenView.Vim'

Plug 'moll/vim-bbye'
Plug 'ap/vim-buftabline'
Plug 'chrisbra/unicode.vim'
Plug 'yssl/QFEnter'

" Plug 'SirVer/ultisnips'
"snippets are separate from engine
Plug 'honza/vim-snippets'
"supertab to allow both youcompleteme and ultisnips to utilize tab button
" Plug 'ervandew/supertab'

Plug 'tommcdo/vim-exchange'
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-commentary'
Plug 'vim-scripts/a.vim'
Plug 'sheerun/vim-polyglot'

Plug 'flazz/vim-colorschemes'
Plug 'ervandew/eclim'
Plug 'Valloric/YouCompleteMe'
Plug 'Yggdroot/indentLine'
Plug 'Houl/vim-repmo'
Plug 'Tuxdude/mark.vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'easymotion/vim-easymotion'
call plug#end()

set omnifunc=syntaxcomplete#Complete
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4

syntax enable
set t_Co=256
colorscheme badwolf
set background=light

set number
set showcmd
set cursorline
set showmatch
set incsearch
" search after replace
set hlsearch

nnoremap B ^
nnoremap E $

nnoremap $ <nop>
nnoremap ^ <nop>

nmap <F1> <nop>
imap <F1> <nop>

let mapleader=","
nnoremap <leader><space> :nohlsearch<CR>

"set indent style in c languages
set cino+=g0

"disable automatic comment contination
autocmd FileType * setlocal formatoptions-=cro
"abbreviations
iabbrev endl << "\n"
iabbrev spacel << " " <<

" ---------------------------------------------------------------------------

"insert newline before/after line without entering insert mode
nnoremap <CR> o<Esc>x

"remap movement keys for split windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

"syntastic settings
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_cpp_compiler = "g++"
let g:syntastic_cpp_compiler_options = "-std=c++14 -O2 -Wall -Wextra -Wpedantic -Wno-vla-extension -Wstrict-prototypes -Wmissing-prototypes"
let g:syntastic_python_checkers=['flake8', 'python3']
let g:syntastic_cpp_include_dirs=['~/repos/lookup/', '../', '../../']
let g:syntastic_c_check_header = 1

" "youcompleteme default configuration file

let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
" "ycm clash with syntastic
" let g:ycm_show_diagnostics_ui=0
let g:ycm_autoclose_preview_window_after_completion = 0
let g:ycm_autoclose_preview_window_after_insertion = 1
" change python version to python3
let g:ycm_python_binary_path = 'usr/local/bin/python3'

"don’t show preview
set completeopt-=preview

"use Ngb to jump to a buffer number
let c = 1
while c <= 99
  execute "nnoremap " . c . "gb :" . c . "b\<CR>"
  let c += 1
endwhile

map <leader>ff :FZF<CR>

nmap <F3> :Obsess<CR>
nmap <F5> :TagbarToggle<CR>
"tagbar
"auto focus when opened and close after selecting a tag
let g:tagbar_autoclose = 1
let g:tagbar_autofocus = 1
let g:tagbar_compact = 1

"GoldenView
let g:goldenview__enable_default_mapping = 0
nmap <F4> <Plug>GoldenViewSplit
nmap <C-o> <Plug>GoldenViewSwitchToggle

function! InsertStatuslineColor(mode)
  if a:mode == 'i'
    hi statusline guibg=Cyan ctermfg=154 guifg=Black ctermbg=0
  elseif a:mode == 'r'
    hi statusline guibg=Purple ctermfg=5 guifg=Black ctermbg=0
  else
    hi statusline guibg=DarkRed ctermfg=1 guifg=Black ctermbg=0
  endif
endfunction

au InsertEnter * call InsertStatuslineColor(v:insertmode)
"au InsertLeave * hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15
au InsertLeave * hi statusline guibg=White ctermfg=15 guifg=Black ctermbg=0

" default the statusline to green when entering Vim
"hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15
hi statusline guibg=White ctermfg=15 guifg=Black ctermbg=0


" Formats the statusline
set statusline=%f                           " file name
"set statusline+=[%{strlen(&fenc)?&fenc:'none'}, "file encoding
"set statusline+=%{&ff}] "file format
set statusline+=%y      "filetype
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag
set statusline+=%{fugitive#statusline()}
" set statusline+=%{ObsessionStatus()}
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
set statusline+=\ %=                        " align left
set statusline+=Line:%l/%L[%p%%]            " line X of Y [percent of file]
set statusline+=\ Col:%c                    " current column
"set statusline+=\ Buf:%n                    " Buffer number
"set statusline+=\ [%b][0x%B]\               " ASCII and byte code under cursor

"vim buftabline
"let g:buftabline_show = 1
let g:buftabline_numbers = 1
"empty area
hi TabLineFill ctermbg=0 guibg=Black ctermfg=0 guifg=Black
"buffer in current window
hi TabLineSel ctermbg=241 guibg=#626262 ctermfg=121 guifg=#87ffaf cterm=bold
"buffer showin in other window
hi PmenuSel ctermbg=239 guibg=#4e4e4e ctermfg=147 guifg=#afafff cterm=NONE
"buffer not currently visible
hi TabLine ctermbg=236 guibg=#303030 ctermfg=189 guifg=#dfdfff
nmap <leader>1 <Plug>BufTabLine.Go(1)
nmap <leader>2 <Plug>BufTabLine.Go(2)
nmap <leader>3 <Plug>BufTabLine.Go(3)
nmap <leader>4 <Plug>BufTabLine.Go(4)
nmap <leader>5 <Plug>BufTabLine.Go(5)
nmap <leader>6 <Plug>BufTabLine.Go(6)
nmap <leader>7 <Plug>BufTabLine.Go(7)
nmap <leader>8 <Plug>BufTabLine.Go(8)
nmap <leader>9 <Plug>BufTabLine.Go(9)
nmap <leader>0 <Plug>BufTabLine.Go(10)

"syntastic settings
let g:syntastic_error_symbol = "\u2717"
let g:syntastic_warning_symbol = "\u26A0"

"unicode completion settings
nmap <F8> <Plug>(MakeDigraph)
nmap ga <Plug>(UnicodeGA)

nnoremap <leader>w :Bdelete<CR>
nnoremap <leader>bd :bd<CR>
nnoremap <leader>e :b<Space>

"------------------------------------------------------------------------------
"ultisnips/ycm tab
"------------------------------------------------------------------------------
" let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
" let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
" let g:SuperTabDefaultCompletionType = '<C-n>'
" let g:UltiSnipsExpandTrigger = '<tab>'
" let g:UltiSnipsJumpForwardTrigger = '<tab>'
" let g:UltiSnipsJumpBackwardTrigger = '<S-tab>'
"------------------------------------------------------------------------------
"ultisnips/ycm tab end
"------------------------------------------------------------------------------

" autocmd VimEnter * Obsession Session.vim

"make j/k navigate to next visual line, for use on long wrapped lines
:nnoremap k gk
:nnoremap j gj
"indented paste in insert mode
"todo: fix this. it interferes with <C-v>endbrace
:inoremap <C-t> <Esc>"+pa
:nnoremap <C-t> "+p
:inoremap <C-]> }<Esc>=<Esc>a

"ycm keeps showing 'user defined completion pattern not found'
"this fixes it
set shortmess+=c

nmap <silent> <C-y> <Plug>GoldenViewResize
autocmd filetype python nnoremap <buffer> <leader>r :w !python3<cr>
autocmd filetype cpp nnoremap <leader>r :w !g++ -std=c++14 -o test.o % -lcurl && ./test.o<cr>
autocmd filetype c nnoremap <leader>r :w !gcc -o test.o % && ./test.o<cr>
autocmd filetype java nnoremap <leader>r :w !javac % && java %:r<cr>
autocmd filetype sh nnoremap <leader>r :w !%:p<cr>

set clipboard=unnamed

filetype on
" au BufReadPost,BufNewFile *.java colorscheme jellybeans
" disable syntastic for java files
let g:syntastic_mode_map = {
    \ "mode": "active",
    \ "passive_filetypes": ["java"] }

" reload from file when focus gained or enter buffer
au FocusGained,BufEnter *.java :silent! !

" indentLine vertical indent
let g:indentLine_char = '¦'
" au BufLeave,FocusLost *.py let g:indentLine_enabled = 0
" au BufEnter,BufNewFile,BufReadPost,FocusGained *.py let g:indentLine_enabled = 1
let g:indentLine_enabled = 0
let g:indentLine_bufNameExclude = ['(?!(.*\.py$))']
let g:indentLine_faster = 1

" hide same lines in vimdiff
:set diffopt=filler,context:0

" map upper case commands to lower case
command WQ wq
command Wq wq
command W w
command Q q

" repomo
" map a motion and its reverse motion:
:noremap <expr> h repmo#Key('h', 'l')|sunmap h
:noremap <expr> l repmo#Key('l', 'h')|sunmap l
" if you like `:noremap j gj', you can keep that:
:noremap <expr> j repmo#Key('j', 'k')|sunmap j
:noremap <expr> k repmo#Key('k', 'j')|sunmap k
" repeat the last [count]motion or the last zap-key:

:noremap <expr> ; repmo#LastKey(';')|sunmap ;
:noremap <expr> <BS> repmo#LastRevKey('')|sunmap <BS>
" add these mappings when repeating with `;' or `,':
:noremap <expr> f repmo#ZapKey('f')|sunmap f
:noremap <expr> F repmo#ZapKey('F')|sunmap F
:noremap <expr> t repmo#ZapKey('t')|sunmap t
:noremap <expr> T repmo#ZapKey('T')|sunmap T

set visualbell
set noerrorbells

" leader-d to delete without copying to buffer
nnoremap <leader>d "_d


let g:EasyMotion_do_mapping = 0 " Disable default mappings
" map <Leader>f <Plug>(easymotion-f)
" map <Leader>j <Plug>(easymotion-j)
" map <Leader>k <Plug>(easymotion-k)
" map <Leader>F <Plug>(easymotion-F)
