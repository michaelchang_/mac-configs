#!/bin/bash

trim() {
    local s2 s="$*"
    # note: the brackets in each of the following two lines contain one space
    # and one tab
    until s2="${s#[    ]}"; [ "$s2" = "$s"  ]; do s="$s2"; done
    until s2="${s%[    ]}"; [ "$s2" = "$s"  ]; do s="$s2"; done
    echo "$s"
}


main() {
    logFile="/tmp/epochLog"
    prev=$(tail -n 1 "$logFile")
    clipW=$(pbpaste)
    clip=$(trim "$clipW")
    len=${#clip}
    regex='^[0-9]*$'

    #filter invalid timestamps
    if [ $len -lt 9 ] || [ $len -gt 13 ] || [ "$clip" == "$prev" ] || ! [[ $clip =~ $regex ]]; then
        exit 1
    fi

    echo $clip >> $logFile
    # valid timestamp
    if [ $len -ge 9 ] && [ $len -le 10 ] && [ "$clip" -ge 631152000 ] && [ "$clip" -le 2240611199 ]
    then
        date=$(date -r "$clip")
        $(terminal-notifier -message "$date" -title "HELLO")
        exit 1
    fi

    if [ $len -ge 12 ] && [ $len -le 13 ] && [ "$clip" -ge 631152000000 ] && [ "$clip" -le 2240611199000 ]
    then
        seconds=$(( clip / 1000 ))
        date=$(date -r "$seconds")
        $(terminal-notifier -message "$date" -title "HELLO")
        exit 1
    fi
}

main
