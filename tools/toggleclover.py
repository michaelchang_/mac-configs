from tempfile import mkstemp
from shutil import move
from os import fdopen, remove
import subprocess

clover_string = "clover.targetPercentage"
beg_comment = "<!-- "
end_comment = " -->"

def replace(file_path):
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if clover_string not in line:
                    new_file.write(line)
                else:
                    # if the line is not commented, comment it
                    if beg_comment not in line:
                        index_after_indent = len(line) - len(line.lstrip())
                        output_line = line[:index_after_indent] \
                                + beg_comment \
                                + line[index_after_indent:].rstrip() \
                                + end_comment \
                                + "\n"
                    else:
                        output_line = line.replace(beg_comment, '').replace(end_comment, '')
                    new_file.write(output_line)

    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)

result = subprocess.Popen("find `pwd` -name pom.xml", shell=True, stdout=subprocess.PIPE).stdout.read()
result = result.decode("utf-8").strip()
pomfiles = result.split("\n")

for pomfile in pomfiles:
    replace(pomfile)
