import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class decompress {

    public static void main(String args[]) throws Exception {
        String s = args[0];
        if (s.endsWith(".hex")) {
            s = new String(Files.readAllBytes(Paths.get(s)));
            s = s.trim();
        }
        byte[] b = hexStringToByteArray(s);
        System.out.println(new String(decompress(b)));
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    /**
     * Decompress ZLIB compressed byte. This method is NOT THREAD SAFE
     */
    public static byte[] decompress(final byte[] zlibCompressedBytes) throws DataFormatException, IOException {

        Inflater decompresser = new Inflater();
        decompresser.setInput(zlibCompressedBytes);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(zlibCompressedBytes.length);
        byte[] buffer = new byte[1024];
        while (!decompresser.finished()) {
            int count = decompresser.inflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        decompresser.reset();
        outputStream.close();
        return outputStream.toByteArray();
    }
}

