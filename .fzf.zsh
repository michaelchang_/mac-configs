# Setup fzf
# ---------
if [[ ! "$PATH" == */Users/mcchang/.fzf/bin* ]]; then
  export PATH="$PATH:/Users/mcchang/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/Users/mcchang/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/Users/mcchang/.fzf/shell/key-bindings.zsh"

