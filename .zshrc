# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
if [ "$TMUX" = "" ]; then tmux; fi

# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="theunraveler"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git osx vi-mode)


source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
  # export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# make commands bold
# zle_highlight=(default:bold)
bindkey -v
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export CLICOLORS=1
export LSCOLORS=exfxcxdxbxegedabagacad
export EDITOR='vim'
DISABLE_UPDATE_PROMPT=true
alias vim="/usr/local/bin/vim"

alias mux="tmuxinator"
alias tmus="tmux"
alias tt="~/tmux-plugins/kill-detached.sh"
export DYLD_LIBRARY_PATH="/usr/local/opt/perl/lib/perl5/5.26.0/darwin-thread-multi-2level/CORE/"

alias grr="git reset --hard head"
alias mvninst="mvn clean install -Dmaven.clover.licenseLocation=/Users/mcchang/clover.license -DskipTests"
alias mvninsttest="mvn clean install -Dmaven.clover.licenseLocation=/Users/mcchang/clover.license"
alias gds="git diff --staged"
alias gdm="git diff master --"

alias cl="clear"
alias vxml="vim -c 'set syntax=xml' -"
alias vjson="vim -c 'set syntax=json' -"
alias vjava="vim -c 'set syntax=java' -"

alias tclover="python ~/tools/toggleclover.py"
alias deptree="mvn dependency:tree | vim -"
alias mvnpng="mvn -U -B exec:java -Denv=qa -Dexec.classpathScope=compile -Dexec.mainClass=\"com.yahoo.boson.core.DotRenderer\";  dot -Tpng descriptor.dot -o descriptor.png"

function decompress() {
    java -classpath ~/tools decompress "$1" | vxml
}

function grepjava() {
    grep "$1" * -ri --include="*.java" --color=always;
}

function grepfile() {
    grep "$1" * -ri --include="$2" --color=always;
}

function gshow() {
    git show master:"$1" | vjava
}

function ggg() {
    branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p');
    if [[ -n $branch ]] && [ $branch != "master" ]; then
        gco master;
        ggl;
        gbd $branch;
    fi
}

function curlhist() {
    COLO=${2:-gq1}
    ENV=${3:-prod}
    MIN=${4:-0}
    MAX=${5:-20220629}
    ROLE=${6:-slave}

    curl -H "Accept: application/json" -G "http://os-warp-yql.v1.production.omega.bf1.yahoo.com:4080/test/v1/finance/redis/zrange/score/TS:$1:HISTORICAL" -d "protoclass=com.yahoo.finance.tachyon.protobuf.TimeSeries\$TSNodePriceVolume" -d "role=$ROLE" -d "colo=$COLO" -d "env=$ENV" -d "min=$MIN" -d "max=$MAX" | jq .| vjson
}

unalias l
alias l="ls -lrth"
unalias glp
alias glp="git log -p"
alias vdes="vim ./src/main/resources/boson/descriptor.json"
alias vset="vim src/main/resources/ycb/settings.json"
alias gbD="git branch -D"
alias md5='md5 -r'
alias md5sum='md5 -r'

alias curltime='curl -o /dev/null -s -w %{time_total}\\n '
alias gituntracked='git ls-files --others --exclude-standard'
alias sshyug1="ssh -A 10.206.143.145"
alias sshyug2="ssh -A 10.206.143.149"
alias sshyug3="ssh -A 10.206.143.154"
alias sshyug4="ssh -A 10.206.143.151"

export FZF_DEFAULT_COMMAND='ag --ignore .git -g ""'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

export SSH_AUTH_SOCK=$HOME/.yubiagent/sock
